# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

class Attack < Action
  def initialize
    
  end
  
  def damage_caused(unit)
    raise NotImplementedError
  end
  
  def past_tense
    raise NotImplementedError
  end
  
  def call
    amount = damage_caused()
    game.message_all("#{unit.name} #{past_tenst} #{target.name} for #{amount} damage.")
    target.hurt(amount)
  end
end
