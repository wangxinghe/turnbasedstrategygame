# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

class Bite < Attack
  def damage_caused
    @unit.teeth
  end
  
  def past_tense
    "bit"
  end
end
